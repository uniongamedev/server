var express  = require('express');
var app      = express();
var http     = require('http').Server(app);
var io       = require('socket.io')(http);

app.use(express.static(__dirname + '/server'));

io.on('connection', function(socket) {
	var id = socket.id;
	console.log('get connection: ' + id);

	socket.on('move', function(data) {
		console.log(data);
		socket.broadcast.emit('move', data);
	});

	socket.on('talk', function(message) {
		socket.broadcast.emit('talk', id, message);
	});

	socket.on('disconnect', function(message) {
		console.log('disconnect: ' + id);
		socket.broadcast.emit('destroy', id);
	});
});

http.listen(3000, function(){
	console.log('listening on *:3000');
});
